## Summary
How do create a private Gem server, using [geminabox](https://github.com/geminabox/geminabox).

## Installation

ssh to host

```bash
$ ssh user@domain.tld
```

create a folder to contain application

```bash
# mkdir geminabox
```

create a subdomain (e.g. geminabox.domain.tld), referencing the folder created in prior step
copy this project's files and folders to server (TODO: configure Capistrano deployment)

```bash
$ rcp path/to/local/project path/to/remote project
```

run Bundler

```bash
# /usr/local/bin/ruby233 /usr/local/bin/bundle233 install --path vendor/bundle`
```

restart application

```bash
# touch tmp/restart.txt
```

visit URL to ensure that geminabox is working

## Usage

edit Gem's `<gemname>.gemspec`; add

```ruby
spec.metadata["allowed_push_host"] = "http://geminabox.domain.tld/"
```

deploy Gem to host

```bash
$ gem inabox -g HOST
```